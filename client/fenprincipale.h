#ifndef FENPRINCIPALE_H
#define FENPRINCIPALE_H

#include <QWidget>

namespace Ui {
class FenPrincipale;
}

class FenPrincipale : public QWidget
{
    Q_OBJECT

public:
    explicit FenPrincipale(QWidget *parent = nullptr);
    ~FenPrincipale();

private:
    Ui::FenPrincipale *ui;
};

#endif // FENPRINCIPALE_H
